package com.lorinit.referencedata.publish;

public interface Subscriber<T> {
    void notified(final T message);
}
