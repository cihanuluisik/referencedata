package com.lorinit.referencedata.publish;

public interface Publisher<T> {

    void publish(final T message);

    void addSubscriber(final Subscriber subscriber);

}
