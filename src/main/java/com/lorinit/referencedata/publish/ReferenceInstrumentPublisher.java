package com.lorinit.referencedata.publish;

import com.lorinit.referencedata.common.Instrument;
import com.lorinit.referencedata.function.InstrumentMergeFunction;
import com.lorinit.referencedata.function.factory.MergeFunctionFactory;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Thread safe. Uses a concurrent hash map merge operation to synchronize concurrent data update
 */
public class ReferenceInstrumentPublisher extends SimpleInstrumentPublisher implements Subscriber<Message> {

    private final Map<String, Instrument> instrumentMap = new ConcurrentHashMap<>();
    private final MergeFunctionFactory mergeFunctionFactory;

    public ReferenceInstrumentPublisher(MergeFunctionFactory mergeFunctionFactory) {
        super("Reference Instrument Publisher");
        Objects.requireNonNull(mergeFunctionFactory, "merge function factory required");
        this.mergeFunctionFactory = mergeFunctionFactory;
    }

    @Override
    public void notified(final Message message) {
        if ( message == null){
                return;
        }
        final InstrumentMergeFunction instrumentMergeFunction = mergeFunctionFactory.createInstrumentMergeFunction(message.getSourceExchange());
        final Instrument arriving = message.getInstrument();
        final Instrument mergedInstrument = instrumentMap.merge(arriving.getKey(), arriving, instrumentMergeFunction);
        publish(new MessageImpl(mergedInstrument, getSource()));
    }


}
