package com.lorinit.referencedata.publish;

import com.lorinit.referencedata.common.Instrument;

/**
 * Created on 13/10/2017.
 */
public interface Message {
    Instrument getInstrument();

    String getSourceExchange();
}
