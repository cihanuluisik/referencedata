package com.lorinit.referencedata.publish;

import com.lorinit.referencedata.common.Instrument;

import java.util.Objects;

public final class MessageImpl implements Message {

    private final Instrument instrument;
    private final String sourceExchange;

    public MessageImpl(Instrument instrument, String sourceExchange) {
        Objects.requireNonNull(sourceExchange, "source exchange required");
        Objects.requireNonNull(instrument, "instrument required");
        this.instrument = instrument;
        this.sourceExchange = sourceExchange;
    }

    @Override
    public Instrument getInstrument() {
        return instrument;
    }

    @Override
    public String getSourceExchange() {
        return sourceExchange;
    }

}
