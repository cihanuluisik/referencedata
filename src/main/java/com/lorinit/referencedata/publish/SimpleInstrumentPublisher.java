package com.lorinit.referencedata.publish;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
* Thread safe. Uses a copy on write array list to subscribers.
 */
public class SimpleInstrumentPublisher implements Publisher<Message> {

    private final List<Subscriber> subscribers = new CopyOnWriteArrayList<>();
    private final String source;

    public SimpleInstrumentPublisher(String exchange) {
        if ( exchange == null || exchange.trim().equals("")){
            throw new NullPointerException("source required");
        }
        this.source = exchange;
    }

    public void addSubscriber(Subscriber subscriber) {
            subscribers.add(subscriber);
    }

    @Override
    public void publish(final Message message) {
            if ( message !=null) {
                subscribers.forEach(subscriber -> subscriber.notified(message));
            }
    }

    public String getSource() {
        return source;
    }
}
