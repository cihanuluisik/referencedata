package com.lorinit.referencedata.function.factory;

import com.lorinit.referencedata.common.InstrumentImpl;
import com.lorinit.referencedata.function.InstrumentMergeFunction;

import static com.lorinit.referencedata.common.ExternalSources.EXCHANGE_LME;
import static com.lorinit.referencedata.common.ExternalSources.EXCHANGE_PRIME;
import static com.lorinit.referencedata.common.Instrument.*;

public class MergeFunctionFactoryImpl implements MergeFunctionFactory {

    /**
     * Creates instrument merge function per exchange source
     * @param exchangeCode
     * @return
     */
    @Override
    public InstrumentMergeFunction createInstrumentMergeFunction(String exchangeCode){
        switch (exchangeCode){
            case EXCHANGE_LME:
                return ( existing, arriving) ->     new InstrumentImpl.InstrumentBuilder()
                                                            .withAllFields(existing)
                                                            .withKeyValue(FIELD_DELIVERY_DATE, arriving.getDeliveryDate())
                                                            .withKeyValue(FIELD_LAST_TRADING_DATE, arriving.getLastTradingDate())
                                                    .build();
            case EXCHANGE_PRIME:
                return (existing, arriving) ->      new InstrumentImpl.InstrumentBuilder()
                                                        .withAllFields(existing)
                                                        .withKeyValue(FIELD_TRADABLE, arriving.getTradable())
                                                        .build();
            default:
                return createDefaultInstrumentMergeFunction();
        }

    }

    /**
     * By default newly arriving instruments overwrites all
     * @return
     */
    public InstrumentMergeFunction createDefaultInstrumentMergeFunction() {
        return (existing, arriving) -> arriving;
    }

}
