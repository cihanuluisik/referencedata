package com.lorinit.referencedata.function.factory;

import com.lorinit.referencedata.function.InstrumentMergeFunction;

public interface MergeFunctionFactory {
    InstrumentMergeFunction createInstrumentMergeFunction(String exchangeCode);
}
