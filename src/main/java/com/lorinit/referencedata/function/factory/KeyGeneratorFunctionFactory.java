package com.lorinit.referencedata.function.factory;

import com.lorinit.referencedata.function.KeyGeneratorFunction;

public interface KeyGeneratorFunctionFactory {
    KeyGeneratorFunction createInstrumentKeyGeneratorFunction(String exchangeCode);
}
