package com.lorinit.referencedata.function.factory;

import com.lorinit.referencedata.common.ExternalSources;
import com.lorinit.referencedata.common.Instrument;
import com.lorinit.referencedata.function.KeyGeneratorFunction;

import static com.lorinit.referencedata.common.ExternalSources.*;

public class KeyGeneratorFunctionFactoryImpl implements KeyGeneratorFunctionFactory {

    /**
     * Creates instrument key generator function per exchange source
     * @param exchangeCode
     * @return
     */
    @Override
    public KeyGeneratorFunction<Instrument> createInstrumentKeyGeneratorFunction(String exchangeCode) {
        switch (exchangeCode){
            case EXCHANGE_LME:
                return (Instrument instr) -> instr.getField(FIELD_LME_CODE).orElse("");
            case EXCHANGE_PRIME:
                return (Instrument instr) -> instr.getField(FIELD_EXCHANGE_CODE).orElse("");
            default:
               return createDefaultInstrumentKeyGeneratorFunction();
        }
    }

    /**
     * By default instrument code used, if null then empty string returned
     * @return
     */
    public KeyGeneratorFunction<Instrument> createDefaultInstrumentKeyGeneratorFunction() {
        return (Instrument instr) -> instr.getField(ExternalSources.FIELD_INSTRUMENT_CODE).orElse("");
    }
}
