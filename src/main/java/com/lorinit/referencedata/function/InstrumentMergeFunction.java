package com.lorinit.referencedata.function;

import com.lorinit.referencedata.common.Instrument;

import java.util.function.BiFunction;

public interface InstrumentMergeFunction extends BiFunction<Instrument, Instrument, Instrument>{
}
