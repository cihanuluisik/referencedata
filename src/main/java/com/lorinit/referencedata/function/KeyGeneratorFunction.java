package com.lorinit.referencedata.function;

@FunctionalInterface
public interface KeyGeneratorFunction<T> {
    String  getKey(T instrument);
}
