package com.lorinit.referencedata.common;

import java.util.Map;
import java.util.Optional;

public interface Instrument {

    //TODO: think about converting to enum
    String FIELD_TRADABLE = "TRADABLE";
    String FIELD_LAST_TRADING_DATE = "LAST_TRADING_DATE";
    String FIELD_DELIVERY_DATE = "DELIVERY_DATE";
    String FIELD_MARKET = "MARKET";
    String FIELD_LABEL = "LABEL";
    String[] REQUIRED_FIELDS = {FIELD_LAST_TRADING_DATE, FIELD_DELIVERY_DATE, FIELD_MARKET, FIELD_LABEL };

    //TODO: think about publishing the format as a field in future to inform subscribers
    String DATE_FORMAT = "dd-MM-yyyy";

    //NOTE: required fields .. thus not optional
    String getKey();
    String getLastTradingDate();
    String getDeliveryDate();
    String getMarket();
    String getLabel();

    // Has default 'TRUE'
    String getTradable();

    Optional<String> getField(String key);

    Map<String, String> getFields();

}
