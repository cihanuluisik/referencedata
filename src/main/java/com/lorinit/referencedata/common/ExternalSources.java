package com.lorinit.referencedata.common;

public final class ExternalSources {

    //TODO: enum ?
    public static final String EXCHANGE_LME = "LME";
    public static final String EXCHANGE_PRIME = "PRIME";
    public static String FIELD_INSTRUMENT_CODE = "INSTRUMENT_CODE";

    private ExternalSources(){};

    //TODO: enum ?
    public static final String FIELD_LME_CODE = "LME_CODE";
    public static final String FIELD_EXCHANGE_CODE = "EXCHANGE_CODE";


}


