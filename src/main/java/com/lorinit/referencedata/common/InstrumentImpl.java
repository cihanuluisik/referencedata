package com.lorinit.referencedata.common;

import com.lorinit.referencedata.function.KeyGeneratorFunction;
import com.lorinit.referencedata.function.factory.KeyGeneratorFunctionFactoryImpl;

import java.util.*;

/**
 * Immutable, safe to share
 */
public final class InstrumentImpl implements Instrument {

    private final Map<String, String> map;
    private final KeyGeneratorFunction<Instrument> keyGeneratorFunction;

    private InstrumentImpl(Map<String, String> map, KeyGeneratorFunction keyGeneratorFunction) {
        this.map = new HashMap<>();
        this.map.putAll(map);
        this.keyGeneratorFunction = keyGeneratorFunction;
    }

    @Override
    public String getTradable() {
        return this.map.getOrDefault(FIELD_TRADABLE, "TRUE");
    }

    @Override
    public String getKey() {
        return keyGeneratorFunction.getKey(this);
    }

    @Override
    public String getLastTradingDate() {
        return map.get(FIELD_LAST_TRADING_DATE);
    }

    @Override
    public String getDeliveryDate() {
        return map.get(FIELD_DELIVERY_DATE);
    }

    @Override
    public String getMarket() {
        return map.get(FIELD_MARKET);
    }

    @Override
    public String getLabel() {
        return map.get(FIELD_LABEL);
    }

    @Override
    public Optional<String> getField(String key) {
        return Optional.ofNullable(map.get(key));
    }

    @Override
    public Map<String, String> getFields() {
        return Collections.unmodifiableMap(map);
    }

    /**
     * Not thread safe
     */
    public static class InstrumentBuilder {
        private Map<String, String> map = new HashMap<>();
        private KeyGeneratorFunction<Instrument> keyGeneratorFunction  = new KeyGeneratorFunctionFactoryImpl().createDefaultInstrumentKeyGeneratorFunction();

        public InstrumentBuilder withKeyValue(String key, String value) {
            this.map.put(key, value);
            return this;
        }

        public Instrument build() {
            Objects.requireNonNull(keyGeneratorFunction, "key generator cant be null");
            Arrays.stream(Instrument.REQUIRED_FIELDS).forEach(fieldName -> Objects.requireNonNull(map.get(fieldName), fieldName + " required"));

            return new InstrumentImpl(map, keyGeneratorFunction);
        }

        public InstrumentBuilder withAllFields(Instrument instrument) {
            this.map.putAll(instrument.getFields());
            return this;
        }

        public InstrumentBuilder withMap(Map<String, String> map) {
            this.map.putAll(map);
            return this;
        }

        public InstrumentBuilder withKeyGenerator(KeyGeneratorFunction<Instrument> keyGeneratorFunction) {
            this.keyGeneratorFunction = keyGeneratorFunction;
            return this;
        }
    }

}
