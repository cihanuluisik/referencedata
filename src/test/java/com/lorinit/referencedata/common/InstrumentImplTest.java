package com.lorinit.referencedata.common;

import com.lorinit.referencedata.InstrumentTestDataHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static com.lorinit.referencedata.common.ExternalSources.FIELD_INSTRUMENT_CODE;
import static com.lorinit.referencedata.common.Instrument.REQUIRED_FIELDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class InstrumentImplTest {

    private Map<String, String> map;
    private String defaultValue;
    private InstrumentTestDataHelper instrumentTestDataHelper  = new InstrumentTestDataHelper();


    @Before
    public void setUp() throws Exception {
        defaultValue = "ValueX";
        map = instrumentTestDataHelper.createInstrumentTestMap(defaultValue);
    }

    @Test
    public void givenNotAllRequiredFieldsConstructionFails() throws Exception {

        String absentField = REQUIRED_FIELDS[0];
        map.remove(absentField);

        assertThat(catchThrowable( () -> instrumentTestDataHelper.getTestInstrumentBuilder(map)
                                    .withKeyGenerator(intr -> "").build()))
                                    .isInstanceOf(NullPointerException.class)
                                    .hasMessage(absentField + " required");
    }

    @Test
    public void givenAllRequiredFieldsThenRequiredValueReadCorrectly() throws Exception {
        Instrument instrument = instrumentTestDataHelper.getTestInstrumentBuilder(map).build();
        Arrays.stream(REQUIRED_FIELDS).forEach(
                fieldName -> assertThat(instrument.getField(fieldName).get()).isEqualTo(defaultValue)
        );
    }


    @Test
    public void givenNoKeyGeneratorThenItUsesDefaultGeneratorWhichReturnsInstrumentCode() throws Exception {
            String instrumentCode = "INSTR_23423432";
            Instrument instrument = instrumentTestDataHelper.getTestInstrumentBuilder(map)
                .withKeyValue(FIELD_INSTRUMENT_CODE, instrumentCode)
                .build();

            assertThat(instrument.getKey()).isEqualTo(instrumentCode);
    }


    @Test
    public void givenAKeyGeneratorThenItUsesGivenGeneratorToGenerateKey() throws Exception {
        String instrumentCode = "INSTR_1";
        String keyPrefix = "KEY_";
        Instrument instrument = instrumentTestDataHelper.getTestInstrumentBuilder(map)
                .withKeyValue(FIELD_INSTRUMENT_CODE, instrumentCode)
                .withKeyGenerator(instr -> keyPrefix + instr.getField(FIELD_INSTRUMENT_CODE).orElse("")
                                                    + instr.getField(Instrument.FIELD_LABEL).orElse("")
                )
                .build();
        assertThat(instrument.getKey()).isEqualTo(keyPrefix + instrumentCode+defaultValue);
    }


}