package com.lorinit.referencedata.publish;

import com.lorinit.referencedata.InstrumentTestDataHelper;
import com.lorinit.referencedata.common.ExternalSources;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

public class SimpleInstrumentPublisherTest {

    private SimpleInstrumentPublisher publisherX;
    private Subscriber<Message> mockedSubscriber;

    @Before
    public void setUp() throws Exception {
        publisherX = new SimpleInstrumentPublisher(ExternalSources.EXCHANGE_PRIME);
        mockedSubscriber = Mockito.mock(Subscriber.class);
        publisherX.addSubscriber(mockedSubscriber);
    }

    @Test
    public void givenSourceNullOrEmptyThenConstructionFails() throws Exception {

        Assertions.assertThat(catchThrowable( () -> new SimpleInstrumentPublisher(null)))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("source required");

        Assertions.assertThat(catchThrowable( () -> new SimpleInstrumentPublisher("")))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("source required");

        Assertions.assertThat(catchThrowable( () -> new SimpleInstrumentPublisher("   ")))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("source required");
    }

    @Test
    public void givenNullMessageThenWontPublish() throws Exception {
        publisherX.publish((Message)null);
        Mockito.verify(mockedSubscriber,Mockito.never()).notified(any(Message.class));
    }

    @Test
    public void givenMessageThenWillPublish() throws Exception {

        final Message message = new MessageImpl(new InstrumentTestDataHelper().getTestInstrument(), ExternalSources.EXCHANGE_LME);

        publisherX.publish(message);

        ArgumentCaptor<Message> argument = ArgumentCaptor.forClass(Message.class);
        Mockito.verify(mockedSubscriber, timeout(1)).notified(any(Message.class));
        verify(mockedSubscriber).notified(argument.capture());
        Message messageComing = argument.getValue();
        assertThat(messageComing).isEqualToComparingFieldByFieldRecursively(message);
    }


}