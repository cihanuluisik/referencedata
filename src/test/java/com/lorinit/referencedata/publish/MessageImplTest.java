package com.lorinit.referencedata.publish;

import com.lorinit.referencedata.InstrumentTestDataHelper;
import com.lorinit.referencedata.common.ExternalSources;
import com.lorinit.referencedata.common.Instrument;
import com.lorinit.referencedata.function.InstrumentMergeFunction;
import com.lorinit.referencedata.function.factory.MergeFunctionFactoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class MessageImplTest {

    private Map<String, String> map;
    private String defaultValue;
    private Instrument instrument;
    private InstrumentMergeFunction defaultInstrumentMergeFunction;
    private InstrumentTestDataHelper instrumentTestDataHelper = new InstrumentTestDataHelper();

    @Before
    public void setUp() throws Exception {
        instrument = instrumentTestDataHelper.getTestInstrument();
        defaultInstrumentMergeFunction = new MergeFunctionFactoryImpl().createDefaultInstrumentMergeFunction();
    }


    @Test
    public void givenNoSourceExchangeThenFailConstruction() throws Exception {
        assertThat(catchThrowable( () -> new MessageImpl(instrument, null)))
                            .isInstanceOf(NullPointerException.class)
                            .hasMessage("source exchange required");
    }

    @Test
    public void givenNoSourceInstrumentThenFailConstruction() throws Exception {
        assertThat(catchThrowable( () -> new MessageImpl(null, ExternalSources.EXCHANGE_LME)))
                            .isInstanceOf(NullPointerException.class)
                            .hasMessage("instrument required");
    }


}