package com.lorinit.referencedata.publish;

import com.lorinit.referencedata.function.factory.MergeFunctionFactoryImpl;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;

public class ReferenceInstrumentPublisherTest {

    private ReferenceInstrumentPublisher referenceInstrumentPublisher;
    private Subscriber<Message> mockedSubscriber;

    @Before
    public void setUp() throws Exception {
        referenceInstrumentPublisher = new ReferenceInstrumentPublisher(new MergeFunctionFactoryImpl());
        mockedSubscriber = Mockito.mock(Subscriber.class);
        referenceInstrumentPublisher.addSubscriber(mockedSubscriber);
    }

    @Test
    public void givenNoMergeFunctionFactoryThenConstructionFails() throws Exception {

        Assertions.assertThat(catchThrowable( () -> new ReferenceInstrumentPublisher(null)))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("merge function factory required");
    }

    @Test
    public void givenNotifiedNullMessageThenWontPublish() throws Exception {
        referenceInstrumentPublisher.notified((Message)null);
        Mockito.verify(mockedSubscriber,Mockito.never()).notified(any(Message.class));
    }


}