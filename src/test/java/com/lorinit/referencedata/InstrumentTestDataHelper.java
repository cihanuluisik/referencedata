package com.lorinit.referencedata;

import com.lorinit.referencedata.common.Instrument;
import com.lorinit.referencedata.common.InstrumentImpl.InstrumentBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.lorinit.referencedata.common.Instrument.REQUIRED_FIELDS;

public class InstrumentTestDataHelper {

    public Map<String, String> createInstrumentTestMap(String defaultValue) {
        Map<String, String>  map = new HashMap<>();
        Arrays.stream(REQUIRED_FIELDS).forEach(f-> map.put(f, defaultValue));
        return map;
    }

    public InstrumentBuilder getTestInstrumentBuilder(Map<String, String> map) {
        return new InstrumentBuilder().withMap(map);
    }

    public Instrument getTestInstrument() {
        final Map<String, String> map = createInstrumentTestMap("ValueX");
        return getTestInstrumentBuilder(map).build();
    }


}

