package com.lorinit.referencedata.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/reference.data.feature"
        ,format = {"pretty","html:reports/service-report"}
        ,glue={"com.lorinit.referencedata.cucumber.steps"}
)
public class ReferenceDataCucumberRunnerTest {


}


