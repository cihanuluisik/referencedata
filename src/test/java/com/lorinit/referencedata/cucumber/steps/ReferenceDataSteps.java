package com.lorinit.referencedata.cucumber.steps;

import com.lorinit.referencedata.common.ExternalSources;
import com.lorinit.referencedata.common.Instrument;
import com.lorinit.referencedata.common.InstrumentImpl;
import com.lorinit.referencedata.function.factory.KeyGeneratorFunctionFactory;
import com.lorinit.referencedata.function.factory.KeyGeneratorFunctionFactoryImpl;
import com.lorinit.referencedata.function.factory.MergeFunctionFactoryImpl;
import com.lorinit.referencedata.publish.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.mockito.ArgumentCaptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ReferenceDataSteps {

    private Map<String, Publisher<Message>> publishers = new HashMap<>();
    private Map<String, Instrument> instrumentsToPublish = new HashMap<>();
    private ReferenceInstrumentPublisher referenceDataPublisher = new ReferenceInstrumentPublisher(new MergeFunctionFactoryImpl());
    private Subscriber testSubscriber ;
    private String publisherSource;
    private KeyGeneratorFunctionFactory keyGeneratorFunctionFactory= new KeyGeneratorFunctionFactoryImpl();

    private void createTestSubscriber() {
        testSubscriber = mock(Subscriber.class);
        referenceDataPublisher.addSubscriber(testSubscriber);
    }

    @Given("^the \"([^\"]*)\" instrument \"([^\"]*)\" with these details:$")
    @And("^a  \"([^\"]*)\" instrument \"([^\"]*)\" with these details:$")
    public void theInstrumentWithTheseDetails(String exchange, String instrumentCode,
                                                List<Map<String, String>> instrumentList) throws Throwable {
        publishers.put(exchange, new SimpleInstrumentPublisher(exchange));
        Instrument instrument = new InstrumentImpl.InstrumentBuilder()
                                    .withMap(instrumentList.get(0))
                                    .withKeyValue(ExternalSources.FIELD_INSTRUMENT_CODE, instrumentCode)
                                    .withKeyGenerator(keyGeneratorFunctionFactory.createInstrumentKeyGeneratorFunction(exchange))
                                    .build();
        instrumentsToPublish.put(instrumentCode, instrument);
    }

    @When("^\"([^\"]*)\" publishes instrument \"([^\"]*)\"$")
    public void publishesInstrument(String exchange, String instrumentCode) throws Throwable {
        this.publisherSource = exchange;
        Publisher publisher = publishers.get(publisherSource);
        publisher.addSubscriber(referenceDataPublisher);
        createTestSubscriber();
        publisher.publish(new MessageImpl(instrumentsToPublish.get(instrumentCode), exchange));
    }


    @Then("^the application publishes the following instrument internally$")
    public void theApplicationPublishesTheFollowingInstrumentInternally(List<Map<String, String>> instrumentList) throws Throwable {
        Instrument expectedInstrument = new InstrumentImpl.InstrumentBuilder()
                .withMap(instrumentList.get(0)).build();

        ArgumentCaptor<Message> argument = ArgumentCaptor.forClass(Message.class);
        verify(testSubscriber).notified(argument.capture());

        Message message = argument.getValue();
        Instrument instrument = message.getInstrument();

        assertThat(instrument.getLastTradingDate()).isEqualTo(expectedInstrument.getLastTradingDate());
        assertThat(instrument.getDeliveryDate()).isEqualTo(expectedInstrument.getDeliveryDate());
        assertThat(instrument.getMarket()).isEqualTo(expectedInstrument.getMarket());
        assertThat(instrument.getLabel()).isEqualTo(expectedInstrument.getLabel());
        assertThat(instrument.getTradable()).isEqualTo(expectedInstrument.getTradable());

        assertThat(message.getSourceExchange()).isNotEqualToIgnoringCase(publisherSource);
        assertThat(message.getSourceExchange()).isEqualToIgnoringCase(referenceDataPublisher.getSource());
    }


}
